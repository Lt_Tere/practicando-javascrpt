//Diferencia entre let y var:
  // let vive entre llaves y var vive entre la funcion

  var i= 5

  for (var i = 0; i < 5;i++) {
    console.log(i);//Devuelve 0,1,2,3,4
  }
  console.log(i)//Fuera del bucle, i vale 5



let i=5
  
for (i = 0; i < 5; i++) {
    console.log(i);
    
}//Esto nos devuelve 1,2,3,4,5.
 //Si buscamos el console.log de i fuera del bucle, nos da error



// Una variable declarada fuera de una función es una variable global y es pasada por referencia a scopes descendientes o herederos

let i = "soy i";
 function a() {
     i = "soy x";
     console.log(i); // Devuelve soy x
 }
 a();
 console.log(i); // Devuelve soy x





//Si declaramos la variable con var también dentro de la función, se crea una variable local cuya visibilidad se reduce a la propia función donde ha sido declarada

var i = "soy i";
function a() {
    var i = "soy x"; // Otra variable local solo para esta función
    console.log(i); // soy x
}
a();
console.log(i); // soy y



function a() {
  var i = "soy i";
}
console.log(i); //Nos devuelve error de que no está definida porque, si es declarada dentro de la funcion, no existe fuera.



//Si intentamos acceder a su valor antes de que se asigne el valor, obtendremos un valor indefinido (undefined),:

console.log(i); // undefined
var i = 1;



//La variable i es una variable global y la variable j es una variable local:

let i = 0;
function a() {
    i = 1;
    let b = 2;
    if(i) {
        console.log(i); // 1
        console.log(b); // 2
    }
}
a();




//Si declaramos una variable con let dentro de una funcion, solo le pertenece a esa funcion. 

function a() {
  let i = 0;
  if(i) {
      let i = 1; // Sería otra variable i solo para el bloque if
      console.log(i); // 1
  }
  console.log(i); // 0
}
a();




//Fuera del bloque con let, la variable no está definida












      
