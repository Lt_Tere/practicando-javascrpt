//Una función sirve como un cierre en JavaScript y, por lo tanto, crea un scope, de modo que (por ejemplo) no se puede acceder a una variable definida exclusivamente dentro de la función desde fuera de la función o dentro de otras funciones. 
//En éste ejemplo puedo acceder a la variable g porque la invoco desde lo más interno.
//La info más interna puede ver lo más externo, pero lo externo no puede ver lo más interno.

function a(){
	let g="soy g"
    console.log (f())
    function f(){
        console.log(g)
    }

}
a() 


//En éste caso la funcion x() accede al console y funcion y()
//la funcion y() accede a lo que haya en la funcion x(), a su console y a la funcion z()
//la funcion z() accede a todo lo de su padres, abuelo y tatarabuelo y a lo interno suyo.

function x(){       
    console.log(y())
    function y(){
        console.log("soy y")
        function z(){
            console.log("soy z")
        }z()

    }
}
x()




